# Contributing

The easiest way to contribute changes to `dfmpy` is to fork the GitLab project,
make the changes local to your forked repository, then submit a Merge Request
through GitLab.

Although there is not formal coding standards used with the `dfmpy` code, your
Merge Request is more likely to be successful if your changes are consistent
with the existing coding style.  This includes indentations, variable/method
names, and even the amount of comments and documentation.

Merge Requests are also more likely to be successful if the commits also include
new unit tests to ensure the new or fixed functionality actually works.  Though
there are _some_ use cases where more tests will not be necessary.  This will be
determined on a case-by-case basis.

When in doubt, create unit tests and add code comments, and add as much
information as you can to the Merge Request.  The main developer will respond as
soon as possible.

Finally, the commands below are intended for the main developer of `dfmpy`, or
anyone given maintainer/developer rights on the GitLab project.

## Development

There are two many ways to locally integration test dfmpy.

1. Locally install via setuptools:  `python3 setup.py install`.  This provides
    the most realistic environment as the installed `dfmpy` command will find
    dfmpy from the installed package (and not the current directory).  However,
    this method can be a bit of a faff when trying to make many one-line changes
    quickly.
2. Or, more simply, directly run the `dfmpy` script from the project directory:
    `cd $DFMPY_ROOT_DIR && ./bin/dfmpy`.  This works because of the way the
    `dfmpy` script is set up as it immediately searches for the `dfmpy` Python
    package which is accessible since the directory exists under the current
    directory.

### A word about "Log Levels"

Generally, the tool follows these rules when logging records:

* **CRITICAL** or **FATAL**:
  * The application encountered a condition that it could not recover from.
  * This is extremely rare, and should be used sparingly.

* **ERROR**:
  * The application encountered an error, but can safely recover.  This is
    usually when the user put the filesystem in a weird state, but still
    something that the tool can recover from.
  * This is another condition that should be used sparingly.

* **WARNING**:
  * This level is used when the tool has to do expected operations to correct
    the state of the filesystem.  For example, if an installed symlink file
    points to a non-existent target, generally it is safe to unlink the
    installed file (since it points to "nothing") and install the correct link
    to the target.

* **INFO**:
  * This level can be useful when trying to determine what the tool is doing at
    a high level.  For example if the tool had to make changes to the filesystem
    that is expected it may log the operation at INFO.  If the filesystem needed
    to be changed such that a file would be overwritten then the tool would log
    that operation at a high level.

* **DEBUG**:
  * This is used for tracing in the code what the tool is doing.  This is very
    detailed information and can easily flood the output to make real warnings
    or errors disappear.  It is advised that this level is only enabled if
    you're making changes to the code or do not understand why the tool
    installed the files it did.

If you find that some log messages do not match this general scheme then
certainly create an Issue on the repository.  This could mean this documentation
needs to be updated, or the actual code needs to be updated.
