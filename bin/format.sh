#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
# set -o xtrace

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${root_dir}"

source "${root_dir}/bin/setup-venv.sh"

python3 -m black    \
    dfmpy/          \
    dfmpy_test/
