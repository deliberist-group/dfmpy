#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
# set -o xtrace

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
venv_dir="${root_dir}/.venv"

if [[ ! -d "${venv_dir}" ]] ; then
    python3 -m venv "${venv_dir}"
fi

# shellcheck disable=SC1091
source "${venv_dir}/bin/activate"

python3 -m pip install                                  \
    --upgrade                                           \
    --requirement "${root_dir}/requirements.txt"        \
    --requirement "${root_dir}/requirements-test.txt"   \
    pip

pushd "${root_dir}"
    python3 -m pip install --editable .
popd
