#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
# set -o xtrace

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${root_dir}"

source "${root_dir}/bin/setup-venv.sh"

python3 -m black --diff --check .
python3 -m pylint .
python3 -m pytest

type shellcheck
find "${root_dir}"              \
    -name '*.sh'                \
    -not -wholename '*/.venv/*' \
    -exec shellcheck {} +

yellow="$(tput setaf 3)"
green="$(tput setaf 2)"
reset="$(tput sgr0)"
echo
echo '########################################'
echo -e "${green}All tests passed.${reset}"
echo -e "${yellow}But check the logs!${reset}"
echo '########################################'
echo
