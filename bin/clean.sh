#!/usr/bin/env bash

set -o errexit
set -o errtrace
set -o noclobber
set -o nounset
set -o pipefail
set -o xtrace

root_dir="$(realpath "$(dirname "${BASH_SOURCE[0]}")/../")"
cd "${root_dir}"

function remove_dir {
    local directory="${1}"
    find .                          \
        -type d                     \
        -name "${directory}"        \
        -not -wholename '*/.venv/*' \
        -exec rm -rfv {} \;         \
        || true
}

remove_dir __pycache__
remove_dir .pytest_cache
remove_dir '*.egg-info'
remove_dir build
