# CHANGELOG

## Version 1

### 1.0.0 - 2024 December 24

- Fixed references to `dfmpy` that should really be `dfm`.
- Removed a number of globs that are no longer needed.
- Changing default config repo path to `~/.local/git/dotfiles/source`.
- Changing tag format to:  release/{new_version}
- Added scripts to help with testing.
- Code formatting and linting updates.
- Added scripts to help with releasing.
- Documentation updates.
- CI enhancements.
- Removed dfm ignored globs.
- Removed usage of `pkg_resources`.
- Fixed imports of with the newer verison of `xdgenvpy`.
- Fixed pylint errors.
- Removed all TODOs requesting to write docs.

## Version 0

### 0.0.5 - 2020 December 22

- Added dates of version release in CHANGELOG.md.
- Renamed LICENSE to LICENSE.md, and included that in the `ignore.globs`.
- Added logo.
- Upgraded xdgenvpy dependency from 2.2.0 to 2.3.4.
- Made code more version 3 Pythonic.
- Upgraded the CI job logic to include linting, unit testing, and code coverage.
- Renamed the installed entry script from `dfmpy` to `dfm`.
- Fixed AppVeyor builds.
- Updated badges on the project.

### 0.0.4 - 2020 January 12

- Added more patterns to the `ignore.globs` config file.
- Updated [xdgenvpy](https://gitlab.com/rbprogrammer/xdgenvpy) dependency.
- Removed custom bin script in favor of setuptools' `entry_points`.
- Moved expected/default dotfiles repo path to `~/.local/git/dotfiles`.
- Updated docs to describe how to create the venv.
- Added AppVeyor CI for MacOS and Windows tests.
- Moved `TODO.md` into Gitlab Issues.
- Added Gitlab Security & Compliance checks during the CI pipelines.
- Forced the requirement of Python 3.6+.
- Updated `setup.py` so it installs dependencies when installed with setuptools.

### 0.0.3 - 2019 November 10

- Made individually "added" files have the most specific quantifier (ie. both
    host and system type).
- Fixed bug with the "init" command where it crashed when backing up existing
    files.
- Refactored the "files" module into a full-fledged package.
- Removed inter-dependencies between command modules.
- Updated a ton of the markdown documentation.

### 0.0.2 - 2019 November 8

- Exposed a more programmatic API, in case a CLI tool is not your forte.
- Added a `--version` CLI switch.
- Added a skeleton unit test, to hopefully stop Gitlab CI from discovering the
    wrong directories that contain unit tests.
- Added the "add" command, which can be used to add a single file into the
    dotfiles repo and sync it with the target environment.
- Renamed the "remove" command to the "uninstall" command, so that the "remove"
    command can be used to remove a single file from the environment.
- Added bumpversion.

### 0.0.1 - 2020 October 23

- Updated project classifiers.
- Added default values to dfmpy ignore globs config.
- Added better logging of CLI arguments and config properties.
- Added more inline TODO statements.
- Trivial updates to docs.

### 0.0.0 - 2019 October 23

- Initial release.
