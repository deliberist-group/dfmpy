# Installation

Since dfmpy is a
[registered Python package on Pypi](https://pypi.org/project/dfmpy), you can
install dfmpy through standard Pip methods.

Install locally as a normal user:

```bash
python3 -m pip install --user --upgrade dfmpy
```

## Initialization

Once installed, dfmpy needs to be initialized with the default set of config
files.  This can simply be done by calling the `init` sub-command.

```bash
dfm init
```

If this is the first time you are initializing dfmpy it will create the
necessary config files under `~/.config/dfmpy`.  Dfmpy strives to be as
filesystem safe as possible.  So it will not overwrite existing config files.

To force the (re)initialization, and potentially overwrite existing files you
can supply the `-f`/`--force` flag.  This will cause dfmpy to reinitialize the
config files with the default templates.  And any existing files will be backed
up.

```bash
dfm init -f
```
