from unittest import TestCase

import dfmpy


class TestSomething(TestCase):

    def test_some_thing(self):
        # pylint: disable=not-callable
        keys = dfmpy.__dict__.keys()
        public_symbols = [s for s in keys if not s.startswith("__")]
        self.assertEqual(0, len(public_symbols))
