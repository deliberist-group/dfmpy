"""
This package does not contain code, per se, but rather a directory for resources
bundled into a dfmpy distribution.  This file makes it easy work for setuptools
to automatically include files from this directory into the bundled dist files.
"""
